#ifndef STACK_H_
#define STACK_H_

#define MAX_SIZE 16

void stack_init();
int stack_push(int value);
int stack_pop();
int stack_full();
int stack_empty();
int stack_search(int value);

#endif /* STACK_H_ */
