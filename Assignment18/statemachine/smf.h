#ifndef SMF_H
#define SMF_H

#include "queue.h"

typedef struct
{
    uint32_t id;
    uint32_t value;
} Event;

enum EventType
{
    SYS_TICK_EVENT,
    KEY_EVENT,
    KEY_EVENT_LONG,
    ACCEL_MEASUREMENT_READY,
    TEMP_MEASUREMENT_READY,
    SPEED_UPDATE_EVENT,
    TEMP_UPDATE_EVENT,
    GRADIENT_UPDATE_EVENT,
    DISTANCE_UPDATE_EVENT,
    CALIBRATION_RECEIVED,
    CLK_UPDATE_EVENT,
    TIMER_UPDATE_EVENT,
    ENERGY_SAVING_ON_EVENT,
    ENERGY_SAVING_OFF_EVENT,
    // add new events here ...

    MAX_EVENT
};

enum TimerEvents
{
    BLINK_TIMER = MAX_EVENT, ADC_MEASUREMENT_TIMER,
// add new timers here ...
};

class SM
{
public:
    virtual void init() = 0;
    virtual void handleEvent(Event event) = 0;
};

class Timer
{
    friend class SMF;

public:
    Timer();
    void set(uint32_t ticks, SM* dest, uint32_t timerID);

private:
    uint32_t m_ticks;
    Timer* m_next;
    uint32_t m_timerID;
    SM* m_dest;
};

class SMF
{
public:
    SMF();
    static void postEvent(uint32_t id, uint32_t value);
    static void processEvents();
    static void subscribe(uint32_t id, SM* sm);
    static void unsubscribe(uint32_t id, SM* sm);
    static void tick();
    static void addTimer(Timer* t);

private:

    enum
    {
        MAX_SM = 4
    };   // max. 4 subscribers per event, should be changed if more are required
    static Queue<Event> eventQueue;
    static Queue<Event> timerQueue;
    static SM* subscriber[MAX_EVENT][MAX_SM];
    static Timer* timerHead;
};

class SMFApp
{
public:
    void run();
    virtual void init() = 0;
    virtual void idle() = 0;

private:
    SMF smf;
};

#endif
