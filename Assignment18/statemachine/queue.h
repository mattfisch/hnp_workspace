#ifndef QUEUE_H
#define QUEUE_H

#include <stdint.h>

template<class T, uint16_t size = 64> class Queue
{
public:

    Queue() :
            m_head(0), m_tail(0)
    {
    }

    void put(const T& t)
    {
        m_buffer[m_tail++] = t;
        if (m_tail == size)
        {
            m_tail = 0;
        }
    }

    T get()
    {
        T t = m_buffer[m_head++];
        if (m_head == size)
        {
            m_head = 0;
        }
        return t;
    }

    bool empty()
    {
        return m_head == m_tail;
    }

private:
    uint16_t m_head;
    uint16_t m_tail;
    T m_buffer[size];
};

#endif
