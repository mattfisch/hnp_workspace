#include <application/application.h>
#include <hardware/hal.h>

void Application::init()
{
    // delay for debugger not need in production
    int i; for(i = 0; i != 1000; i++);

    // turn off all IO ports
    PA->DIR = 0xFFFF;
    PB->DIR = 0xFFFF;
    PC->DIR = 0xFFFF;
    PD->DIR = 0xFFFF;
    PE->DIR = 0xFFFF;
    PJ->DIR = 0xFFFF;
    PA->OUT = 0;
    PB->OUT = 0;
    PC->OUT = 0;
    PD->OUT = 0;
    PE->OUT = 0;
    PJ->OUT = 0;

    MAP_WDT_A_holdTimer();

    // set the core voltage level to VCORE1 (switching to 48 MHz)
    MAP_PCM_setCoreVoltageLevel(PCM_VCORE1);

    // set 1 flash wait states for Flash bank 0 and 1 (48 MHz)
    MAP_FlashCtl_setWaitState(FLASH_BANK0, 1);
    MAP_FlashCtl_setWaitState(FLASH_BANK1, 1);

    // initialize clock system
    MAP_CS_setDCOCenteredFrequency (CS_DCO_FREQUENCY_48);
    MAP_CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1); // MCLK = 48 MHz
    MAP_CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1); // SMCLK = 48 MHz (SPI)

    keypad.init();
    hmi.init();
    blinkingLED.init();
    adcManager.init();
    temperatureSensor.init();
    gradientSensor.init();
    CalibrationIF calibrationIF;
    timerManager.init();

    __deep_sleep();
}

void Application::idle()
{
    __deep_sleep();
}

