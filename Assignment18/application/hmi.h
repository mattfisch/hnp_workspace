#ifndef HMI_H
#define HMI_H

#include "statemachine/smf.h"
#include "driver/TimerManager.h"
#include "driver/keypad.h"
#include "driver/LCD.h"
#include <cstring>

#define MAX_STRING_LENGTH 1000

class HMI: public SM
{
public:
    HMI();
    ~HMI();

    void init();
    void handleEvent(Event evt);

private:
    enum State
    {
        HMI_STATE_SPEED,
        HMI_STATE_DISTANCE,
        HMI_STATE_TEMPERATURE,
        HMI_STATE_GRADIENT
    };
    State state;

    LCD lcd;

    void displaySpeed();
    void displayDistance();
    void displayTemperature();
    void displayGradient();
    void displayTime();
};

#endif
