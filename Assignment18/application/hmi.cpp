#include <application/hmi.h>

#include <stdio.h>

HMI::HMI() :
        state(HMI_STATE_GRADIENT)
{
    SMF::subscribe(KEY_EVENT, this);
    SMF::subscribe(TEMP_UPDATE_EVENT, this);
    SMF::subscribe(GRADIENT_UPDATE_EVENT, this);
    SMF::subscribe(SPEED_UPDATE_EVENT, this);
    SMF::subscribe(DISTANCE_UPDATE_EVENT, this);
    SMF::subscribe(TIMER_UPDATE_EVENT, this);
    SMF::subscribe(CLK_UPDATE_EVENT, this);
}

HMI::~HMI()
{
}

void HMI::init()
{
    lcd.init();
    displayGradient();
    displayTime();
    lcd.turnOn();
}

const char *concat(const char *str1, const char *str2)
{
    static char buffer[MAX_STRING_LENGTH];
    std::strncpy(buffer, str1, MAX_STRING_LENGTH);
    if (std::strlen(str1) < MAX_STRING_LENGTH)
    {
        std::strncat(buffer, str2, MAX_STRING_LENGTH - std::strlen(buffer));
    }
    buffer[MAX_STRING_LENGTH - 1] = '\0';
    return buffer;
}

char *itoa(int value, char *result, int base)
{
    // check that the base if valid
    if (base < 2 || base > 36)
    {
        *result = '\0';
        return result;
    }

    char* ptr = result, *ptr1 = result, tmp_char;
    int tmp_value;

    do
    {
        tmp_value = value;
        value /= base;
        *ptr++ =
                "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz"[35
                        + (tmp_value - value * base)];
    }
    while (value);

    // Apply negative sign
    if (tmp_value < 0)
        *ptr++ = '-';
    *ptr-- = '\0';
    while (ptr1 < ptr)
    {
        tmp_char = *ptr;
        *ptr-- = *ptr1;
        *ptr1++ = tmp_char;
    }
    return result;
}

void HMI::handleEvent(Event evt)
{
    switch (state)
    {
    case HMI_STATE_SPEED:
        switch (evt.id)
        {
        case KEY_EVENT:
            if (evt.value == Keypad::BTN1_LONGPRESS)
            {
                displayDistance();
                state = HMI_STATE_DISTANCE;
            }
            else if (evt.value == Keypad::BTN1_PRESS)
            {
                displayGradient();
                state = HMI_STATE_GRADIENT;
            }
            else if (evt.value == Keypad::BTN2_PRESS)
            {
                //BUTTON 2 PRESSED
            }
            else if (evt.value == Keypad::BTN2_LONGPRESS)
            {
                //BUTTON 2 LONG PRESSED
            }
            break;
        case SPEED_UPDATE_EVENT:
            displaySpeed();

            char speedBuf[10];

            const char* str = "  ";
            str = concat(str, itoa(evt.value / 10, speedBuf, 10));
            str = concat(str, ".");
            str = concat(str, itoa(evt.value % 10, speedBuf, 10));
            str = concat(str, "      ");
            lcd.drawString16(str, 10, 48, 0);

            state = HMI_STATE_SPEED;
            break;
        }
        break;
//##################################################################################
    case HMI_STATE_DISTANCE:
        if (evt.id == KEY_EVENT && evt.value == Keypad::BTN1_LONGPRESS)
        {
            displayTemperature();
            state = HMI_STATE_TEMPERATURE;
        }
        else if (evt.id == KEY_EVENT && evt.value == Keypad::BTN1_PRESS)
        {
            displaySpeed();
            state = HMI_STATE_SPEED;
        }
        else if (evt.value == Keypad::BTN2_PRESS)
        {
            // BUTTON 2 PRESSED
        }
        else if (evt.value == Keypad::BTN2_LONGPRESS)
        {
            // BUTTON 2 LONG PRESSED
        }
        else if (evt.id == DISTANCE_UPDATE_EVENT)
        {
            int meter = evt.value / 1000;
            char distBuf[5];
            const char* str = "  ";
            str = concat(str, itoa(meter, distBuf, 10));
            str = concat(str, "     ");
            lcd.drawString16(str, 10, 48, 0);

            state = HMI_STATE_DISTANCE;
        }
        break;
//##################################################################################
    case HMI_STATE_TEMPERATURE:

        if (evt.id == KEY_EVENT && evt.value == Keypad::BTN1_LONGPRESS)
        {
            displayGradient();
            state = HMI_STATE_GRADIENT;
        }
        else if (evt.id == KEY_EVENT && evt.value == Keypad::BTN1_PRESS)
        {
            displayDistance();
            state = HMI_STATE_DISTANCE;
        }
        else if (evt.value == Keypad::BTN2_PRESS)
        {
            // BUTTON 2 PRESSED
        }
        else if (evt.value == Keypad::BTN2_LONGPRESS)
        {
            // BUTTON 2 LONG PRESSED
        }
        else if (evt.id == TEMP_UPDATE_EVENT)
        {
            displayTemperature();
            int deg1 = (evt.value / 1000);
            int deg2 = (evt.value % 100);

            char temp1[3];
            char temp2[3];
            const char *str = concat(itoa(deg1, temp1, 10), ".");
            str = concat(str, itoa(deg2, temp2, 10));
            //the semicolon is representing a °
            str = concat(str, ";C      ");

            lcd.drawString16(str, 10, 48, 0);
            state = HMI_STATE_TEMPERATURE;
        }
        break;
//##################################################################################
    case HMI_STATE_GRADIENT:
        if (evt.id == KEY_EVENT && evt.value == Keypad::BTN1_LONGPRESS)
        {
            state = HMI_STATE_SPEED;
            displaySpeed();
        }
        else if (evt.id == KEY_EVENT && evt.value == Keypad::BTN1_PRESS)
        {
            state = HMI_STATE_TEMPERATURE;
            displayTemperature();
        }
        else if (evt.id == GRADIENT_UPDATE_EVENT)
        {
            displayGradient();
            char grad[3];
            //the semicolon is representing a °
            const char* str = "  ";
            str = concat(str, itoa(evt.value, grad, 10));
            str = concat(str, ";       ");
            lcd.drawString16(str, 10, 48, 0);
            state = HMI_STATE_GRADIENT;
        }
        else if (evt.value == Keypad::BTN2_PRESS)
        {
            // BUTTON 2 PRESSED
        }
        else if (evt.value == Keypad::BTN2_LONGPRESS)
        {
            // BUTTON 2 LONG PRESSED
        }
        break;
    }
    if (evt.id == CLK_UPDATE_EVENT)
    {
        const char * str = "";
        char buffer[2];
        // seconds to time
        int hours = evt.value / 3600;
        int minutes = (evt.value % 3600) / 60;
        int seconds = (evt.value % 3600) % 60;

        // hours
        str = concat(str, itoa(hours, buffer, 10));
        str = concat(str, ":");
        // minutes
        str = concat(str, itoa(minutes, buffer, 10));
        // seconds
        str = concat(str, ":");
        str = concat(str, itoa(seconds, buffer, 10));
        str = concat(str, "    ");
        lcd.drawString8(str, 50, 118, 0);
    }
}

void HMI::displaySpeed()
{
    lcd.drawString8("SPEED       ", 10, 10, 0);
}

void HMI::displayDistance()
{
    lcd.drawString8("DISTANCE   ", 10, 10, 0);
}

void HMI::displayTemperature()
{
    lcd.drawString8("TEMPERATURE", 10, 10, 0);
}

void HMI::displayGradient()
{
    lcd.drawString8("GRADIENT   ", 10, 10, 0);
}

void HMI::displayTime()
{
    lcd.drawString8("TIME", 10, 118, 0);
}
