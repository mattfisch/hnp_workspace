#ifndef APPLICATION_H
#define APPLICATION_H

#include "hmi.h"
#include "driver/keypad.h"
#include "driver/blinkingled.h"
#include "driver/ADCManager.h"
#include "driver/CalibrationIF.h"
#include "driver/FlashTemperatureStorage.h"
#include "driver/TimerManager.h"
#include "driver/sensor/temperatureSensor.h"
#include "driver/sensor/gradientSensor.h"
#include "driver/sensor/SpeedSensor.h"


class Application: public SMFApp
{
public:
    void init();
    void idle();

private:
    Keypad keypad;
    HMI hmi;
    BlinkingLED blinkingLED;
    ADCManager adcManager;
    TemperatureSensor temperatureSensor;
    GradientSensor gradientSensor;
    TimerManager timerManager;
};

#endif
