#include <application/application.h>
#include <hardware/hal.h>
#include "statemachine/smf.h"

int main()
{
    Application app;
    app.run();
}

/*
 * Measure the current consumption of your application and let EnergyTrace
 * estimate how long the system could run with a CR2032 coin cell,
 *
 * Power consumption with __deep_sleep() and display 100%
 * Mean: 153,4199mW             46,4909mA
 * Min:  93,9932mW              28,4828mA
 * Max:  183,9044mW             55,7286mA
 * Battery Life: 0,2 days (est.)
 *
 * Power consumption with __deep_sleep() and display 0% after 5 seconds
 * Mean: 115,4394mW             34,9816mA
 * Min:  74,3678mW              22,5357mA
 * Max:  182,5352mW             55,3137mA
 * Battery Life: 0,2 days (est.)
 *
 * When using energy saving mode (Turning of the LED backlight and turning off
 * the timer for sampling and debouncing the buttons) we save ~11,5093mA.
 *
 * ########################################################################################
 *
 * Study the schematics of the MK II board. How would you have to change the
 * hardware to implement a reasonable low-power mode with this board?
 *
 * Deactivate all LED's not used by the application (Even though this is not possible)
 * To further reduce energy consumption, the launchpad should be disconnected from the
 * MK II and only the PINS used should be connected to the launchpad.
 */
