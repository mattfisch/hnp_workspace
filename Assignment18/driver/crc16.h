#ifndef CRC16_H
#define CRC16_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif
uint16_t crc16(uint8_t *data, uint16_t size, uint16_t crc);
#ifdef __cplusplus
}
#endif

#endif

