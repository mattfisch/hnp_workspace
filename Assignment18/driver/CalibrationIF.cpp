#include <driver/CalibrationIF.h>

#define PREAMBLE 0xFF
#define MAX_LENGTH 2
#define STATE_FRAME_START 0
#define STATE_FRAME_LENGTH 1
#define STATE_FRAME_DATA 2
#define STATE_FRAME_CHECKSUM 3

uint8_t state = STATE_FRAME_START;
uint8_t length;
uint8_t data[MAX_LENGTH];
uint8_t checksum[2];
uint8_t i = 0;
uint8_t dataNew[3];

void UART_Handler(void)
{
    if (MAP_UART_getInterruptStatus(EUSCI_A0_BASE,
    EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG) == EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG)
    {
        switch (state)
        {
        case STATE_FRAME_START:
            if (MAP_UART_receiveData(EUSCI_A0_BASE) == PREAMBLE)
            {
                state = STATE_FRAME_LENGTH;
            }
            break;
        case STATE_FRAME_LENGTH:
            length = MAP_UART_receiveData(EUSCI_A0_BASE);
            state = STATE_FRAME_DATA;
            break;
        case STATE_FRAME_DATA:
            data[i++] = MAP_UART_receiveData(EUSCI_A0_BASE);
            if (i == length)
            {
                i = 0;
                state = STATE_FRAME_CHECKSUM;
            }
            break;
        case STATE_FRAME_CHECKSUM:
            checksum[i++] = MAP_UART_receiveData(EUSCI_A0_BASE);

            if (i > 1)
            {
                dataNew[0] = length;
                dataNew[1] = data[0];
                dataNew[2] = data[1];

                uint16_t check = crc16(dataNew, sizeof(dataNew), 0);
                uint16_t checksumInt = (checksum[1] << 8) | checksum[0];

                if (check == checksumInt)
                {
                    SMF::postEvent(CALIBRATION_RECEIVED, (uint32_t) (data[1] << 8) | data[0]);
                }
                i = 0;
                state = STATE_FRAME_START;
            }
            break;
        }
    }
}

CalibrationIF::CalibrationIF()
{
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(
            GPIO_PORT_P1, GPIO_PIN2 | GPIO_PIN3, GPIO_PRIMARY_MODULE_FUNCTION);
    MAP_UART_initModule(EUSCI_A0_BASE, &uartConfig);
    MAP_UART_enableModule(EUSCI_A0_BASE);
    MAP_UART_registerInterrupt(EUSCI_A0_BASE, UART_Handler);
    MAP_UART_enableInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT); // UART module interrupt enable
    MAP_Interrupt_enableInterrupt(INT_EUSCIA0); // NVIC interrupt enable
}

CalibrationIF::~CalibrationIF()
{
}

