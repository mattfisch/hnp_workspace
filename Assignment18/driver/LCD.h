#ifndef LCD_H
#define LCD_H

#include <stdint.h>
#include "hardware/hal.h"
#include "statemachine/smf.h"

class LCD: public SM
{
public:
    LCD();
    ~LCD();

    void init(void);
    void handleEvent(Event);

    void setBackLight(uint8_t intensity); // 0 - 100; (off - maximum intensity)
    void turnOn();
    void turnOff();
    void clearScreen();

    void drawPixel(uint8_t x, uint8_t y, uint16_t color);

    void drawChar8(char c, uint8_t x, uint8_t y, uint16_t color);
    void drawString8(const char* str, uint8_t x, uint8_t y, uint16_t color);

    void drawChar16(char c, uint8_t x, uint8_t y, uint16_t color);
    void drawString16(const char* str, uint8_t x, uint8_t y, uint16_t color);

private:
    void writeCommand(uint8_t command);
    void writeData(uint8_t data);
    void setDrawFrame(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1);

    static const uint16_t NOT_SET[16];

    enum
    {
        LCD_CHAR_8_WIDTH = 8,
        LCD_CHAR_8_HEIGHT = 8,
        LCD_8_MAX_CHARACTERS = 96,
        LCD_CHAR_16_WIDTH = 16,
        LCD_CHAR_16_HEIGHT = 16,
        LCD_16_MAX_CHARACTERS = 36
    };

    static const uint8_t font8[LCD_8_MAX_CHARACTERS][LCD_CHAR_8_WIDTH];

    static const uint16_t font16[LCD_16_MAX_CHARACTERS][LCD_CHAR_16_WIDTH];
};

#endif
