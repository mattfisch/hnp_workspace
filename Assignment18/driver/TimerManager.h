#ifndef DRIVER_TIMERMANAGER_H_
#define DRIVER_TIMERMANAGER_H_

#include "statemachine/smf.h"
#include "hardware/hal.h"

#define TIRE_CIRCUMFERENCE int(2105)
#define ACLK int(32768)

class TimerManager: public SM
{
public:
    TimerManager();
    virtual ~TimerManager();
    void init();
    void handleEvent(Event evt);
private:
    int value;
    int oldValue;
};

#endif /* DRIVER_TIMERMANAGER_H_ */
