#ifndef DRIVER_SENSOR_SPEEDSENSOR_H_
#define DRIVER_SENSOR_SPEEDSENSOR_H_

#include "statemachine/smf.h"

class SpeedSensor: public SM
{
public:
    SpeedSensor();
    virtual ~SpeedSensor();
    void init();
    void handleEvent(Event evt);
private:
    int value;
    int oldValue;
};

#endif /* DRIVER_SENSOR_SPEEDSENSOR_H_ */
