/*
 * SpeedSensor.cpp
 *
 *  Created on: 30.05.2017
 *      Author: GWU
 */

#include <driver/sensor/SpeedSensor.h>

SpeedSensor::SpeedSensor() : value(0), oldValue(0)
{
    SMF::subscribe(SPEED_UPDATE_EVENT, this);

}

SpeedSensor::~SpeedSensor()
{
    // TODO Auto-generated destructor stub
}

void SpeedSensor::init()
{
   // nothing to do here
}

void SpeedSensor::handleEvent(Event evt)
{

}
