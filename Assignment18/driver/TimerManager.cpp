#include <driver/TimerManager.h>

Timer_A_ContinuousModeConfig continousConfig = {
    TIMER_A_CLOCKSOURCE_ACLK,
    TIMER_A_CLOCKSOURCE_DIVIDER_1,
    TIMER_A_TAIE_INTERRUPT_ENABLE,
    TIMER_A_SKIP_CLEAR, };

Timer_A_CaptureModeConfig captureConfig = {
        TIMER_A_CAPTURECOMPARE_REGISTER_4,
        TIMER_A_CAPTUREMODE_RISING_EDGE,
        TIMER_A_CAPTURE_INPUTSELECT_CCIxA,
        TIMER_A_CAPTURE_ASYNCHRONOUS,
        TIMER_A_CAPTURECOMPARE_INTERRUPT_ENABLE,
        TIMER_A_OUTPUTMODE_OUTBITVALUE };

uint32_t overflows;
uint32_t counts;
uint32_t captureResult;

RTC_C_Calendar curTime;

const RTC_C_Calendar initialTime = { 0, // 00 seconds
        50, // 50 minutes
        8, // 08 hours
        8, // day: 8th
        5, // month: May
        2017 // year: 2017
        };

TimerManager::TimerManager() :
        value(0), oldValue(0)
{
    // TODO Auto-generated constructor stub

}

TimerManager::~TimerManager()
{
    // TODO Auto-generated destructor stub
}

void RTC_C_Handler()
{
    if (RTC_C_getInterruptStatus() & RTC_C_CLOCK_READ_READY_INTERRUPT)
    {
        curTime = RTC_C_getCalendarTime();
        int seconds = curTime.seconds + curTime.minutes * 60
                + curTime.hours * 3600;

        SMF::postEvent(CLK_UPDATE_EVENT, seconds);
        MAP_RTC_C_clearInterruptFlag(RTC_C_CLOCK_READ_READY_INTERRUPT);
    }
}

void CaptureAndOverflowHandler()
{
    static int distance = 0;

    if (Timer_A_getInterruptStatus(
    TIMER_A2_BASE) == TIMER_A_INTERRUPT_PENDING)
    {
        // overflow
        overflows++;
        counts = overflows * 0xFFFF;
        MAP_Timer_A_clearInterruptFlag( TIMER_A2_BASE);
    }
    if ((MAP_Timer_A_getCaptureCompareInterruptStatus(
            TIMER_A2_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_4,
            TIMER_A_CAPTURECOMPARE_INTERRUPT_FLAG)
            == TIMER_A_CAPTURECOMPARE_INTERRUPT_FLAG))
    {
        // capture complete
        captureResult = MAP_Timer_A_getCaptureCompareCount(
                TIMER_A2_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_4);
        Timer_A_clearTimer(TIMER_A2_BASE);
        counts = overflows * 0xFFFF + captureResult;
        overflows = 0;
        MAP_Timer_A_clearCaptureCompareInterrupt(
                TIMER_A2_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_4);
    }

    int speed = (TIRE_CIRCUMFERENCE * ACLK * 36) / (counts * 1000) ;
    distance += TIRE_CIRCUMFERENCE;

    SMF::postEvent(SPEED_UPDATE_EVENT, speed);
    SMF::postEvent(DISTANCE_UPDATE_EVENT, distance);
}

void TimerManager::init()
{
    // continuous mode
    MAP_GPIO_setAsPeripheralModuleFunctionOutputPin(
            GPIO_PORT_PJ, GPIO_PIN0 | GPIO_PIN1, GPIO_PRIMARY_MODULE_FUNCTION);
    MAP_CS_startLFXT(CS_LFXT_DRIVE3);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0);

    // start real time clock
    MAP_RTC_C_holdClock();
    MAP_RTC_C_initCalendar(&initialTime, RTC_C_FORMAT_BINARY);
    MAP_RTC_C_registerInterrupt(RTC_C_Handler);
    MAP_RTC_C_enableInterrupt(RTC_C_CLOCK_READ_READY_INTERRUPT); // enable 1 second interval interrupts
    MAP_RTC_C_startClock();

    // capture mode
    MAP_GPIO_setAsInputPin(GPIO_PORT_P6, GPIO_PIN7);
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(
            GPIO_PORT_P6, GPIO_PIN7, GPIO_PRIMARY_MODULE_FUNCTION);

    // continuous mode
    MAP_Timer_A_configureContinuousMode(TIMER_A2_BASE, &continousConfig);
    // capture mode interrupt
    MAP_Timer_A_enableCaptureCompareInterrupt(
            TIMER_A2_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_4);
    // init capture mode
    MAP_Timer_A_initCapture(TIMER_A2_BASE, &captureConfig);
    // register interrupts for both
    MAP_Timer_A_registerInterrupt(TIMER_A2_BASE,
    TIMER_A_CCRX_AND_OVERFLOW_INTERRUPT,
                                  CaptureAndOverflowHandler);
    // start timer counter
    Timer_A_startCounter(TIMER_A2_BASE, TIMER_A_CONTINUOUS_MODE);
}

void TimerManager::handleEvent(Event evt)
{

}
