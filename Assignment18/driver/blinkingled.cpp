#include <driver/blinkingled.h>
#include <hardware/hal.h>

BlinkingLED::BlinkingLED() :
        state(TOGGLING)
{
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN0);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN0);

    blinkTimer.set(50, this, BLINK_TIMER);
}

BlinkingLED::~BlinkingLED()
{
}

void BlinkingLED::init()
{
}

void BlinkingLED::handleEvent(Event evt)
{
    switch (state)
    {
    case TOGGLING:
        blinkTimer.set(50, this, BLINK_TIMER);
        toggle();
        break;
    default:
        break;
    }
}

void BlinkingLED::toggle()
{
    GPIO_toggleOutputOnPin(GPIO_PORT_P2, GPIO_PIN0);
}
