//*****************************************************************************
//
// MSP432 main.c template - Empty main
//
//****************************************************************************

#include "msp.h"

#include <driverlib/driverlib.h>

int clock_state;

void assignment_7()
{
    P2->DIR |= 0x40;      // set bit 6 (LED) to output
    P2->OUT &= ~0x40;     // turn led off

    MAP_CS_setDCOCenteredFrequency(CS_DCO_FREQUENCY_3);

    clock_state = 0;
}

void assignment_8()
{

    P2->DIR |= 0x10;      // set bit 5 (LED) to output
    P2->OUT &= ~0x10;     // turn led off

    __disable_irq();

    P3->REN = 0x20;       // set button to send 1 when pressed
    P3->OUT = 0x20;       // enable button output
    P3->IES = 0x20;       // interrupt when button is 1 (pressed)

    P3->IFG = 0;          // clear all P3 interrupt flags
    P3->IE = 0x20;        // enable interrupt in the GPIO modul

    __enable_irq();

    NVIC_EnableIRQ(PORT3_IRQn);
}

assignment_9()
{
    P5->DIR |= 0x40;      // set bit 5 (LED) to output
    P5->OUT &= ~0x40;     // turn led off

    SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk; // enable SysTick
    SysTick->LOAD = 600000 - 1; // reload with 49999
    SysTick->VAL = 0x01; // clear (dummy write)
    SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk; // enable SysTick interrupt
}

void main(void)
{
    assignment_7();
    assignment_8();
    assignment_9();

    int counter = 0;
    for (;;)
    {
        //assignment7 part
        if (counter == 50000)
        {
            P2->OUT ^= 0x40;
            counter = 0;
        }
        counter++;
    }
}

// P2 Interrupt service routine
void PORT3_IRQHandler(void)
{
    //delay to make toggling smoother
    int i = 0;
    for (i; i < 50000; i++)
    {

    }

    // read P3 ->IV to acknowledge interrupt
    if (!(P3->IV & 0x20))
    {
        //assignment 8 part
        P2->OUT ^= 0x10;        //BIT5 -> toggle LED on or off

        //assignment 7 part
        if (!clock_state)
        {
            MAP_CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT,
            CS_CLOCK_DIVIDER_4);
            clock_state = 1;
        }
        else
        {
            MAP_CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT,
            CS_CLOCK_DIVIDER_1);
            clock_state = 0;
        }
    }

    //delay to make toggling smoother
    i = 0;
    for (i; i < 50000; i++)
    {

    }
}

// ISR
void SysTick_Handler(void)
{
    P5->OUT ^= 0x40;
}
