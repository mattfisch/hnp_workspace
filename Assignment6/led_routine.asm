	.global start

	.text

start:
 	movw.w r1, #0x4C44				; P5 DIR
 	movt.w r1, #0x4000				; P5 DIR
    ldrb r2, [r1]					; Load registry
    orr r3, r2, #0x40				; OR combine with 01000000
    strb r3, [r1]					; save register

    movw.w r1, #0x4C42				; P5 OUT
    movt.w r1, #0x4000				; P5 OUT
    ldrb r2, [r1]					; Load registry
    and r3, r2, #0xBF				; LED blue, pin 6, turned off
    strb r3, [r1]					; save register

loop:
	movw.w r1, #0x4C40				; P5.1 IN
	movt.w r1, #0x4000				; P5.1 IN
	ldrb r2, [r1]					; Load registry
	lsrs r2, r2, #2					; shift the two LSB out
	bhs led_off						; jump to led_off if true
	b led_on							; else jump to led_on

led_off:
	movw.w r1, #0x4C42				; P5 OUT
    movt.w r1, #0x4000				; P5 OUT
    ldrb r2, [r1]					; Load registry
    and r3, r2, #0xBF				; AND combine with 10111111
    strb r3, [r1]					; save register
	b loop							; jump to loop

led_on:
	movw.w r1, #0x4C42				; P5 OUT
    movt.w r1, #0x4000				; P5 OUT
    ldrb r2, [r1]					; Load registry
    orr r3, r2, #0x40				; OR combine with 01000000
    strb r3, [r1]					; save register
    b loop							; jump to loop
