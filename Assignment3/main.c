//*****************************************************************************
//
// MSP432 main.c template - Empty main
//
//****************************************************************************

#include "assert.h"

void main(void)
{
    int v;

    queue_init();

    assert(queue_empty());

    queue_put(1);

    assert(queue_get(&v) && v == 1);

    queue_put(2);

    assert(queue_get(&v) && v == 2);

    queue_put(3);

    assert(queue_get(&v) && v == 3);

    queue_put(4);

    assert(queue_get(&v) && v == 4);

    queue_put(5);

    assert(queue_get(&v) && v == 5);

    queue_put(1);

    assert(queue_get(&v) && v == 1);

    queue_put(2);

    assert(queue_get(&v) && v == 2);

    queue_put(3);

    assert(queue_get(&v) && v == 3);

    queue_put(4);

    assert(queue_get(&v) && v == 4);

    queue_put(5);

    assert(queue_get(&v) && v == 5);

    queue_put(1);

    assert(queue_get(&v) && v == 1);

    queue_put(2);

    assert(queue_get(&v) && v == 2);

    queue_put(3);

    assert(queue_get(&v) && v == 3);

    queue_put(4);

    assert(queue_get(&v) && v == 4);

    queue_put(5);

    assert(queue_get(&v) && v == 5);

    queue_put(1);

    assert(queue_get(&v) && v == 1);

    queue_put(2);

    assert(queue_get(&v) && v == 2);

    queue_put(3);

    assert(queue_get(&v) && v == 3);

    queue_put(4);

    assert(queue_get(&v) && v == 4);

    queue_put(5);

    assert(queue_get(&v) && v == 5);

    queue_put(1);

    assert(queue_get(&v) && v == 1);

    queue_put(2);

    assert(queue_get(&v) && v == 2);

    queue_put(3);

    assert(queue_get(&v) && v == 3);

    queue_put(4);

    assert(queue_get(&v) && v == 4);

    queue_put(5);

    assert(queue_get(&v) && v == 5);

    queue_put(1);

    assert(queue_get(&v) && v == 1);

    queue_put(2);

    assert(queue_get(&v) && v == 2);

    queue_put(3);

    assert(queue_get(&v) && v == 3);

    queue_put(4);

    assert(queue_get(&v) && v == 4);

    queue_put(5);

    assert(queue_get(&v) && v == 5);

    return 0;
}
