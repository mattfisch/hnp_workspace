/*
 * queue.c
 *
 *  Created on: 8 May 2017
 *      Author: Matthias Fischbacher
 */

#include "queue.h"

int head;
int tail;
int swapped;
int queue[QUEUE_MAX_SIZE];

void queue_init()
{
    head = 0;
    tail = 0;
    swapped = 0;
}

int queue_put(int v)
{
    if(tail == QUEUE_MAX_SIZE && head == 0)
    {
     return 0;
    }
    else if(tail == QUEUE_MAX_SIZE)
    {
      tail = 0;
      swapped = 1;
    }
    if(swapped == 1 && tail == head)
      return 0;

    queue[tail++] = v;
    return 1;
}

int queue_get(int* v)
{
    if(queue_empty() == 0)
    {
        if(head == (QUEUE_MAX_SIZE-1))
        {
        *v = queue[head];
        head = 0;
        swapped = 0;
    }
    else
    {
        *v = queue[head++];
    }
        return 1;
    }
    return 0;
}

int queue_empty()
{
    if(head == tail && swapped == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}


