#include <assert.h>

int main()
{
    queue_init();
    assert(queue_empty());

    int v;

    queue_init();

    assert(queue_empty());

    queue_put(1);
    queue_put(2);
    queue_put(3);
    queue_put(4);
    queue_put(5);

    assert(queue_put(6) == 0);

    assert(queue_get(&v) && v == 1);

    assert(queue_put(11) == 1);

    assert(queue_get(&v) && v == 2);
    assert(queue_get(&v) && v == 3);

    assert(queue_put(22) == 1);
    assert(queue_put(33) == 1);

    assert(queue_get(&v) && v == 4);
    assert(queue_get(&v) && v == 5);

    assert(queue_put(44) == 1);
    assert(queue_put(55) == 1);

    assert(queue_get(&v) && v == 11);
    assert(queue_get(&v) && v == 22);

    assert(queue_put(111) == 1);
    assert(queue_put(222) == 1);

    assert(queue_get(&v) && v == 33);
    assert(queue_get(&v) && v == 44);
    assert(queue_get(&v) && v == 55);
    assert(queue_get(&v) && v == 111);
    assert(queue_get(&v) && v == 222);

    assert(queue_empty());

    return 0;
}
