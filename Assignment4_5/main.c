//*****************************************************************************
//
// MSP432 main.c template - Empty main
//
//****************************************************************************

#include "msp.h"
#include <driverlib/driverlib.h>

void assignment_1()
{
    if (!(P5->IN & 0x02))
    {
        P5->OUT |= 0x40;
    }
    else
    {
        P5->OUT &= ~0x40;
    }
}

int assignment_2(int toggled)
{
    if (!(P3IN & 0x20))
    {
        if (toggled == 0)
        {
            P2OUT &= ~0x10;
            return 1;
        }
        else
        {
            P2OUT |= 0x10;
            return 0;
        }
    }
    return toggled;
}

int assignment_3(int delay_counter)
{
    delay_counter++;
    if( delay_counter == 30000)
    {
        MAP_GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN6);
    }
    if(delay_counter == 60000)
    {
        MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN6);
        delay_counter = 0;
    }
    return delay_counter;
}

void main(void)
{
    WDTCTL = WDTPW | WDTHOLD;           // Stop watchdog timer

    // Assignment 4.1
    P5->DIR |= 0x40;            // Select LED for output
    P5->OUT &= ~0x40;           // turn off LED

    // Assignment 4.2
    P2DIR |= 0x10;
    P2OUT &= ~0x10;

    // Assignment 4.3
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN6);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN6);

    int toggled = 0;
    int delay_counter = 0;
    for (;;)
    {
        assignment_1();

        toggled = assignment_2(toggled);

        delay_counter = assignment_3(delay_counter);
    }
}
