#include "driver/sensor/temperatureSensor.h"

#define ABSOLUTE_ZERO int(-274)

TemperatureSensor::TemperatureSensor() :
        value(ABSOLUTE_ZERO), oldValue(ABSOLUTE_ZERO), offset(ABSOLUTE_ZERO), oldOffset(ABSOLUTE_ZERO)
{
    SMF::subscribe(TEMP_MEASUREMENT_READY, this);
    SMF::subscribe(CALIBRATION_RECEIVED, this);
}

TemperatureSensor::~TemperatureSensor()
{
}

void TemperatureSensor::init()
{
    offset = oldOffset = tempStorage.readOffset();
}

void TemperatureSensor::handleEvent(Event evt)
{
    switch (evt.id)
    {
    case (TEMP_MEASUREMENT_READY):
    {
        int adcRefTempCal_1_2v_30 = TLV->ADC14_REF1P2V_TS30C;
        int adcRefTempCal_1_2v_85 = TLV->ADC14_REF1P2V_TS85C;
        value = (((int) MAP_ADC14_getResult(ADC_MEM0) - adcRefTempCal_1_2v_30)
                * (85 - 30) * 1000)
                / (adcRefTempCal_1_2v_85 - adcRefTempCal_1_2v_30) + (30 * 1000);
        if (value != oldValue)
        {
            SMF::postEvent(TEMP_UPDATE_EVENT,
                           offset == ABSOLUTE_ZERO ? value : value - offset);
            oldValue = value;
        }
    }
        break;
    case (CALIBRATION_RECEIVED):
    {
        if (value != ABSOLUTE_ZERO)
        {
            // since the temperature is multiplied by a factor of 1000 to avoid floating point
            // calculations and the calibration value received is multiplied by 10,
            // to receive the real values, the received value must be multiplied by 100.
            offset = value - (evt.value * 100);

        }
        if( oldOffset != offset) {
            tempStorage.writeOffset(offset);
            oldOffset = offset;
        }
    }
        break;
    }
}

