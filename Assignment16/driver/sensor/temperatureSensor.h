#ifndef DRIVER_TEMPERATURESENSOR_H_
#define DRIVER_TEMPERATURESENSOR_H_

#include "statemachine/smf.h"
#include "driver/ADCManager.h"
#include "driver/FlashTemperatureStorage.h"
#include "hardware/hal.h"


class TemperatureSensor: public SM
{
public:
    TemperatureSensor();
    ~TemperatureSensor();
    void init();
    void handleEvent(Event evt);
private:
    FlashTemperatureStorage tempStorage;
    int value;
    int oldValue;
    int offset;
    int oldOffset;
};

#endif
