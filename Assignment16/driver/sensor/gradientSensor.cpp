#include <driver/sensor/gradientSensor.h>

GradientSensor::GradientSensor() :
        value(0), oldValue(0)
{
    SMF::subscribe(ACCEL_MEASUREMENT_READY, this);
}

GradientSensor::~GradientSensor()
{
}

void GradientSensor::init()
{
    //nothing to do here
}

void GradientSensor::handleEvent(Event evt)
{
    value = (-1000 * (((int) (evt.value >> 16)) - 8192)
            / (((int) (evt.value & 0xFFFF)) - 8192));
    if (value > 0)
    {
        value = (value + 5) / 10;
    }
    else
    {
        value = (value - 5) / 10;
    }
    if (value > 100)
        value = 100;
    if (value < -100)
        value = -100;
    if (oldValue != value)
    {
        SMF::postEvent(GRADIENT_UPDATE_EVENT, value);
        oldValue = value;
    }
}
