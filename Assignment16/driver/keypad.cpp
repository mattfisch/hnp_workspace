#include <hardware/hal.h>
#include "keypad.h"
#include "statemachine/smf.h"

uint16_t Keypad::shiftRegister;
uint16_t Keypad::debouncedKey;

Keypad::Keypad()
{

}

void Timer32_Handler()
{
    Keypad::scan();
    MAP_Timer32_clearInterruptFlag(TIMER32_0_BASE);
}

void Keypad::init()
{
    GPIO_setAsInputPin(GPIO_PORT_P5, GPIO_PIN1);
    GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P5, GPIO_PIN1);

    GPIO_setAsInputPin(GPIO_PORT_P3, GPIO_PIN5);
    GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P3, GPIO_PIN5);

    MAP_Timer32_initModule(TIMER32_0_BASE, TIMER32_PRESCALER_1, TIMER32_32BIT,
    TIMER32_PERIODIC_MODE);
    MAP_Timer32_setCount(TIMER32_0_BASE, 240000); // 48 MHz, 5ms --> 240000
    MAP_Timer32_registerInterrupt(TIMER32_0_INTERRUPT, Timer32_Handler);
    MAP_Timer32_enableInterrupt(TIMER32_0_BASE);
    MAP_Timer32_startTimer(TIMER32_0_BASE, 0);
    MAP_Interrupt_enableInterrupt(INT_T32_INT1);
}

void Keypad::handleEvent(Event event)
{
}

void Keypad::scan()
{
    static int counter = 0;
    static int event_captured = 0;
    static int pressed_btn = 0;

    if (!GPIO_getInputPinValue(GPIO_PORT_P5, GPIO_PIN1)
            || !GPIO_getInputPinValue(GPIO_PORT_P3, GPIO_PIN5))
    {
        shiftRegister = (shiftRegister << 1) | 0x01;
    }
    else
    {
        shiftRegister = shiftRegister << 1;
    }

    if ((shiftRegister & 0x3F) == 0x3F)
    {
        // store that a button press was detected
        debouncedKey = 1;

        if (!BITBAND_PERI(P5->IN, 1))
        {
            pressed_btn = 1;
        }
        else
        {
            pressed_btn = 2;
        }

        counter++;
        if (!event_captured && counter == 300)
        {
            if (!BITBAND_PERI(P5->IN, 1))
                SMF::postEvent(KEY_EVENT, BTN1_LONGPRESS);
            else
                SMF::postEvent(KEY_EVENT, BTN2_LONGPRESS);
            event_captured = 1;
            counter = 0;
        }
    }
    else if ((shiftRegister & 0xF0) == 0xF0)
    {
        // signal is 0 again (button was released on not bouncing)
        if (!event_captured && debouncedKey == 1)
        {
            // a falling edge was detected - publish that information
            if (pressed_btn == 1)
            {
                SMF::postEvent(KEY_EVENT, BTN1_PRESS);
                event_captured = 1;
            }
            else if (pressed_btn == 2)
            {
                SMF::postEvent(KEY_EVENT, BTN2_PRESS);
                event_captured = 1;
            }
        }
        counter = 0;
        event_captured = 0;
        debouncedKey = 0;
    }
}

