#include <driver/ADCManager.h>
#include "hardware/hal.h"

ADCManager::ADCManager()
{
    timer.set(20, this, ADC_MEASUREMENT_TIMER);
}

ADCManager::~ADCManager()
{
}

void ADC14_Handler(void)
{
    SMF::postEvent(TEMP_MEASUREMENT_READY, MAP_ADC14_getResult(ADC_MEM0));
    SMF::postEvent(
            ACCEL_MEASUREMENT_READY,
            (MAP_ADC14_getResult(ADC_MEM1) << 16)
                    | (MAP_ADC14_getResult(ADC_MEM2) & 0xFFFF));
}

void ADCManager::init()
{
    // y axis
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P4, GPIO_PIN0,
    GPIO_TERTIARY_MODULE_FUNCTION);

    // z axis
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P4, GPIO_PIN2,
    GPIO_TERTIARY_MODULE_FUNCTION);

    // wait until ref generator is not in use by a peripheral
    while (MAP_REF_A_isRefGenBusy())
    {
    }

    // set to 1.2 V
    MAP_REF_A_setReferenceVoltage (REF_A_VREF1_2V);

    // enable reference generator
    MAP_REF_A_enableReferenceVoltage();
    MAP_REF_A_enableTempSensor();

    MAP_ADC14_enableModule();
    MAP_ADC14_initModule(ADC_CLOCKSOURCE_ADCOSC, ADC_PREDIVIDER_64,
    ADC_DIVIDER_8,
                         ADC_TEMPSENSEMAP);

    // start conversion manually with ADC12SC bit
    MAP_ADC14_enableSampleTimer(ADC_MANUAL_ITERATION);
    MAP_ADC14_setSampleHoldTime(ADC_PULSE_WIDTH_192, ADC_PULSE_WIDTH_192);

    //temperature
    MAP_ADC14_configureConversionMemory(ADC_MEM0,
    ADC_VREFPOS_INTBUF_VREFNEG_VSS,
                                        ADC_INPUT_A22,
                                        ADC_NONDIFFERENTIAL_INPUTS);

    //accelerometer y
    MAP_ADC14_configureConversionMemory(ADC_MEM1, ADC_VREFPOS_AVCC_VREFNEG_VSS,
    ADC_INPUT_A13,
                                        ADC_NONDIFFERENTIAL_INPUTS);

    //accelerometer z
    MAP_ADC14_configureConversionMemory(ADC_MEM2, ADC_VREFPOS_AVCC_VREFNEG_VSS,
    ADC_INPUT_A11,
                                        ADC_NONDIFFERENTIAL_INPUTS);

    MAP_ADC14_configureMultiSequenceMode(ADC_MEM0, ADC_MEM2, false);

    MAP_ADC14_enableInterrupt (ADC_INT2);
    MAP_ADC14_registerInterrupt(ADC14_Handler);

    MAP_ADC14_enableConversion();
}

void ADCManager::handleEvent(Event evt)
{
    timer.set(20, this, ADC_MEASUREMENT_TIMER);
    MAP_ADC14_toggleConversionTrigger();
}
