#ifndef DRIVER_FLASHTEMPERATURESTORAGE_H_
#define DRIVER_FLASHTEMPERATURESTORAGE_H_

#include "driver/crc16.h"
#include "hardware/hal.h"

#define FLASH_START 0x200000
#define FLASH_END 0x200FFF
#define SEGMENT_FREE 0xFFFF
#define SEGMENT_USED 0xFEFE
#define SEGMENT_VALID 0xFCFC
#define SEGMENT_OLD 0xF8F8

typedef struct {
    uint16_t status;
    uint16_t crc16;
} FlashBlockHeader;


typedef struct {
    FlashBlockHeader header;
    int offset;
} FlashBlock;


class FlashTemperatureStorage
{
public:
    FlashTemperatureStorage();
    virtual ~FlashTemperatureStorage();
    int readOffset();
    bool writeOffset(int);

private:
    bool check_crc(FlashBlock* block);

};

#endif /* DRIVER_FLASHTEMPERATURESTORAGE_H_ */
