#ifndef HMI_H
#define HMI_H

#include "statemachine/smf.h"

class HMI : public SM
{
public:
    HMI();
    ~HMI();

    void init();
    void handleEvent(Event evt);

private:

private:
    enum State {HMI_STATE_SPEED, HMI_STATE_DISTANCE, HMI_STATE_TEMPERATURE, HMI_STATE_GRADIENT};
    State state;

    void displaySpeed();
    void displayDistance();
    void displayTemperature();
    void displayGradient();
    void displayTime();
};

#endif
