#include <application/hmi.h>
#include "driver/keypad.h"
#include <stdio.h>

HMI::HMI() :
        state(HMI_STATE_GRADIENT)
{
    SMF::subscribe(KEY_EVENT, this);
    SMF::subscribe(TEMP_UPDATE_EVENT, this);
    SMF::subscribe(GRADIENT_UPDATE_EVENT, this);
    SMF::subscribe(SPEED_UPDATE_EVENT, this);
    SMF::subscribe(DISTANCE_UPDATE_EVENT, this);
}

HMI::~HMI()
{
}

void HMI::init()
{
}

void HMI::handleEvent(Event evt)
{
    switch (state)
    {
    case HMI_STATE_SPEED:
        switch (evt.id)
        {
        case KEY_EVENT:
            if (evt.value == Keypad::BTN1_LONGPRESS)
            {
                displayDistance();
                state = HMI_STATE_DISTANCE;
            }
            else if (evt.value == Keypad::BTN1_PRESS)
            {
                displayGradient();
                state = HMI_STATE_GRADIENT;
            }
            else if (evt.value == Keypad::BTN2_PRESS)
            {
                printf("btn2 press\n");
            }
            else if (evt.value == Keypad::BTN2_LONGPRESS)
            {
                printf("btn2 longpress\n");
            }
            break;
        case SPEED_UPDATE_EVENT:
            displaySpeed();
            state = HMI_STATE_SPEED;
            break;
        }
        break;
//##################################################################################
    case HMI_STATE_DISTANCE:
        if (evt.id == KEY_EVENT && evt.value == Keypad::BTN1_LONGPRESS)
        {
            displayTemperature();
            state = HMI_STATE_TEMPERATURE;
        }
        else if (evt.id == KEY_EVENT && evt.value == Keypad::BTN1_PRESS)
        {
            displaySpeed();
            state = HMI_STATE_SPEED;
        }
        else if (evt.value == Keypad::BTN2_PRESS)
        {
            printf("btn2 press\n");
        }
        else if (evt.value == Keypad::BTN2_LONGPRESS)
        {
            printf("btn2 longpress\n");
        }
        break;
//##################################################################################
    case HMI_STATE_TEMPERATURE:

        if (evt.id == KEY_EVENT && evt.value == Keypad::BTN1_LONGPRESS)
        {
            displayGradient();
            state = HMI_STATE_GRADIENT;
        }
        else if (evt.id == KEY_EVENT && evt.value == Keypad::BTN1_PRESS)
        {
            displayDistance();
            state = HMI_STATE_DISTANCE;
        }
        else if (evt.value == Keypad::BTN2_PRESS)
        {
            printf("btn2 press\n");
        }
        else if (evt.value == Keypad::BTN2_LONGPRESS)
        {
            printf("btn2 longpress\n");
        }
        else if (evt.id == TEMP_UPDATE_EVENT)
        {
            displayTemperature();
            printf("Temperature: %d.%d\n", evt.value / 1000, evt.value % 1000);
            state = HMI_STATE_TEMPERATURE;
        }
        break;
//##################################################################################
    case HMI_STATE_GRADIENT:
        if (evt.id == KEY_EVENT && evt.value == Keypad::BTN1_LONGPRESS)
        {
            state = HMI_STATE_SPEED;
            displaySpeed();
        }
        else if (evt.id == KEY_EVENT && evt.value == Keypad::BTN1_PRESS)
        {
            state = HMI_STATE_TEMPERATURE;
            displayTemperature();
        }
        else if (evt.id == GRADIENT_UPDATE_EVENT)
        {
            displayGradient();
            printf("Gradient: %d\n", evt.value);
            state = HMI_STATE_GRADIENT;
        }
        else if (evt.value == Keypad::BTN2_PRESS)
        {
            printf("btn2 press\n");
        }
        else if (evt.value == Keypad::BTN2_LONGPRESS)
        {
            printf("btn2 longpress\n");
        }
        break;
    }

    displayTime();
}

void HMI::displaySpeed()
{
    printf("SPEED\n");
}

void HMI::displayDistance()
{
    printf("DISTANCE\n");
}

void HMI::displayTemperature()
{
    printf("TEMPERATURE\n");
}

void HMI::displayGradient()
{
    printf("GRADIENT\n");
}

void HMI::displayTime()
{
    printf("TIME\n");
}
