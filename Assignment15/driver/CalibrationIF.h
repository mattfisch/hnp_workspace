#ifndef DRIVER_CALIBRATIONIF_H_
#define DRIVER_CALIBRATIONIF_H_

#include "statemachine/smf.h"
#include "hardware/hal.h"
#include "crc16.h"
#include <stdio.h>

const eUSCI_UART_Config uartConfig = {
        // SMCLK Clock Source (48 MHz)
        EUSCI_A_UART_CLOCKSOURCE_SMCLK,
        // BRDIV = 312
        312,
        // UCxBRF = 19
        19,
        // UCxBRS = 0
        0,
        // no Parity
        EUSCI_A_UART_NO_PARITY,
        // MSB first
        EUSCI_A_UART_LSB_FIRST,
        // one stop bit
        EUSCI_A_UART_ONE_STOP_BIT,
        // UART mode
        EUSCI_A_UART_MODE,
        // oversampling
        EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION
        };

class CalibrationIF
{
public:
    CalibrationIF();
    virtual ~CalibrationIF();
};

#endif /* DRIVER_CALIBRATIONIF_H_ */
