#include <driver/FlashTemperatureStorage.h>

FlashTemperatureStorage::FlashTemperatureStorage()
{
}

FlashTemperatureStorage::~FlashTemperatureStorage()
{
}

bool FlashTemperatureStorage::check_crc(FlashBlock* block)
{
    return crc16((uint8_t*) block->offset, sizeof(block->offset), 0) == block->header.crc16;
}

int FlashTemperatureStorage::readOffset()
{
    FlashBlock* curBlock = (FlashBlock*) FLASH_START;
    // Find the first valid entry
    while (curBlock->header.status != SEGMENT_VALID
            && curBlock < (FlashBlock*) FLASH_END)
    {
        curBlock++;
    }
    // Return a pointer to the data block
    if (curBlock < (FlashBlock*) FLASH_END && check_crc(curBlock))
    {
        return curBlock->offset; // if we are within our sector, return a pointer to the data
    }
    else
    {
        return 0; // no valid entry found, return default data
    }
}

bool FlashTemperatureStorage::writeOffset(int offset)
{
    FlashBlock* curBlock = (FlashBlock*) FLASH_START;
    FlashBlock* oldBlock;
    FlashBlockHeader header;
    if (!ROM_FlashCtl_unprotectSector(FLASH_INFO_MEMORY_SPACE_BANK0,
    FLASH_SECTOR0))
    {
        return 0;
    }
    // Find the first free entry
    while (curBlock->header.status != SEGMENT_FREE
            && curBlock < (FlashBlock*) FLASH_END)
    {
        oldBlock = curBlock;
        curBlock++;
    }
    // Write data, possibly erase before writeFree entry found
    if (curBlock < (FlashBlock*) FLASH_END
            && ((uint8_t*) FLASH_END - (uint8_t*) curBlock)
                    >= sizeof(FlashBlock))
    {

        // free block found
        // write header and data
        header.status = SEGMENT_USED;
        header.crc16 = crc16((uint8_t*) &offset, sizeof(offset), 0);
        if(ROM_FlashCtl_programMemory(&header, &curBlock->header,sizeof(FlashBlockHeader)))
        {
            if(ROM_FlashCtl_programMemory(&offset, &curBlock->offset, sizeof(offset)))
            {
                header.status = SEGMENT_VALID;
                if(!ROM_FlashCtl_programMemory(&header.status, &curBlock->header.status,
                                sizeof(header.status)))
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }

        if (oldBlock != 0)
        {
            header.status = SEGMENT_OLD;
            ROM_FlashCtl_programMemory(&header.status, &oldBlock->header.status,
                                       sizeof(header.status));
        }
    }
    else
    {
        // segment is full � erase, then write
        ROM_FlashCtl_eraseSector(FLASH_START);
        curBlock = (FlashBlock*) FLASH_START;
        header.status = SEGMENT_USED;

        header.crc16 = crc16((uint8_t*) &offset, sizeof(offset), 0);

        ROM_FlashCtl_programMemory(&header, &curBlock->header,
                                   sizeof(FlashBlockHeader));
        ROM_FlashCtl_programMemory(&offset, &curBlock->offset, sizeof(offset));
        header.status = SEGMENT_VALID;
        ROM_FlashCtl_programMemory(&header.status, &curBlock->header.status,
                                   sizeof(header.status));
    }
    ROM_FlashCtl_protectSector(FLASH_INFO_MEMORY_SPACE_BANK0, FLASH_SECTOR0);
    return 1;
}
