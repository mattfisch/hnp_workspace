#ifndef BLINKINGLED_H_
#define BLINKINGLED_H_

#include "statemachine/smf.h"

class BlinkingLED: public SM
{
public:
    BlinkingLED();
    ~BlinkingLED();

    void init();
    void handleEvent(Event evt);

private:
    Timer blinkTimer;
    void toggle();

    enum State
    {
        TOGGLING
    };
    State state;

};

#endif
