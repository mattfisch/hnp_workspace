#ifndef KEYPAD_H
#define KEYPAD_H

#include <stdint.h>
#include "statemachine/smf.h"

class Keypad: public SM
{

public:
    typedef struct
    {
        int short_press_event;
        int long_press_event;
    } Button;

    Keypad();

    void init();

    static void scan();

    static void checkButton(int, int, Button);

    void handleEvent(Event event);

    enum
    {
        BTN1_PRESS = 0, BTN1_LONGPRESS = 1, BTN2_PRESS = 2, BTN2_LONGPRESS = 3,
    };

private:
    static uint16_t shiftRegister;
    static uint16_t debouncedKey;
};

#endif
