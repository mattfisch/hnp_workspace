#include "smf.h"
#include <stdint.h>
#include <assert.h>
#include <hardware/hal.h>
#include <stdio.h>

Queue<Event> SMF::eventQueue;
Queue<Event> SMF::timerQueue;
SM* SMF::subscriber[MAX_EVENT][MAX_SM];
Timer* SMF::timerHead;

extern "C"
{

void SysTick_Handler(void)
{
    SMF::postEvent(SYS_TICK_EVENT, 0);
    SMF::tick();
}
}

Timer::Timer()
{
    m_ticks = 0;
    m_next = 0;
}

void Timer::set(unsigned int ticks, SM* dest, unsigned int timerID)
{
    m_ticks = ticks;
    m_timerID = timerID;
    m_dest = dest;
    SMF::addTimer(this);
}

SMF::SMF()
{
    SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk; // enable SysTick

    SysTick->LOAD = 480000 - 1;                     // reload with 10 ms @ 48MHz
    SysTick->VAL = 0x01;                       // clear by writing a dummy value
    SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;       // Enable SysTick interrupt
}

void SMF::postEvent(uint32_t id, uint32_t value)
{
    __disable_irq();
    Event evt = { id, value };
    eventQueue.put(evt);
    __enable_irq();
}

void SMF::processEvents()
{
    Event curEvent;
    Event curTimer;

    while (!eventQueue.empty())
    {
        curEvent = eventQueue.get();
        for (int i = 0; i != MAX_SM; i++)
        {
            if (subscriber[curEvent.id][i] != 0)
            {
                subscriber[curEvent.id][i]->handleEvent(curEvent);
            }
        }
    }

    while (!timerQueue.empty())
    {
        curTimer = timerQueue.get();
        ((SM*) curTimer.value)->handleEvent(curTimer);
    }
}

void SMF::subscribe(uint32_t id, SM* sm)
{
    unsigned int i;

    for (i = 0; subscriber[id][i] != 0 && i != MAX_SM; i++)
        ;
    if (i != MAX_SM)
    {
        subscriber[id][i] = sm;
    }
}

void SMF::unsubscribe(uint32_t id, SM* sm)
{
    unsigned int i;

    for (i = 0; subscriber[id][i] != sm && i != MAX_SM; i++)
        ;
    if (i != MAX_SM)
    {
        subscriber[id][i] = 0;
    }
}

void SMF::tick()
{
    Timer* cur = timerHead;
    Timer* prev = timerHead;

    while (cur != 0)
    {
        cur->m_ticks--;
        if (cur->m_ticks == 0)
        {
            Event timerEvent = { cur->m_timerID, (unsigned int) cur->m_dest };
            timerQueue.put(timerEvent);
            if (cur == timerHead)
            {
                timerHead = cur->m_next;
                prev = cur = timerHead;
            }
            else
            {
                prev->m_next = cur->m_next;
                cur = cur->m_next;
            }
        }
        else
        {
            prev = cur;
            cur = cur->m_next;
        }

    }
}

void SMF::addTimer(Timer* t)
{
    __disable_irq();
    t->m_next = timerHead;
    timerHead = t;
    __enable_irq();
}

void SMFApp::run()
{
    init();

    for (;;)
    {
        SMF::processEvents();
        idle();
    }
}

