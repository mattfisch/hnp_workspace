; ---------------------------------------------------------------------------
;
; Queue implementation in TI ASM
;
; ---------------------------------------------------------------------------
; Global definitions of subroutines
	.global queue_init, queue_put, queue_get, queue_empty

; ---------------------------------------------------------------------------
; Define stack entries
MAX_SIZE .equ 4*4

	.bss queue, MAX_SIZE, 4
; ---------------------------------------------------------------------------
	.data
head .word 0					; HEAD
tail .word 0 					; TAIL
elements .word 0				; elements

; ---------------------------------------------------------------------------
	.text
hp .word head				; constant to head
tp .word tail				; constant to tail
qp .word queue				; constant to queue
ep .word elements			; constant to elements

; ---------------------------------------------------------------------------
; Initialize queue
; ---------------------------------------------------------------------------
queue_init:
	mov r0, #0
	ldr r1, hp				; head = 0
	str r0, [r1]
	ldr r1, tp				; tail = 0
	str r0, [r1]
	ldr r1, ep				; elements = 0
	str r0, [r1]
	bx lr					; return to caller


; ---------------------------------------------------------------------------
; Insert value in Queue
;
; parameter value is stored in R0
; ---------------------------------------------------------------------------
queue_put:
	ldr r1, tp				; if tail == MAX_SIZE
	ldr r2, [r1]
	cmp r2, #MAX_SIZE
	beq check_head			; jump to check head
							; else
	ldr r1, hp
	ldr r3, [r1]
	ldr r4, [r2, #4]			; tail + 1
	cmp r4, r3				; if tail + 1 != head
	bne insert				; insert value
	b exit_error				; else exit with error

insert:
	ldr r3, qp				; load queue
	str r0, [r3, r2]			; store value in queue on position tail

	ldr r0, ep				; load elements pointer
	ldr r1, [r0]				; dereference elementsptr into r1
	add r1, #4				; increase elements by one
	str r1, [r0]				; store elements value

	ldr r1, tp				; get tail pointer
	ldr r2, [r1]				; dereference tail into r2
	add r2, #4				; increment tail by one
	cmp r2, #MAX_SIZE		; test if tail pointer is out of bounds
	beq set_tail_zero		; jump to set_tail_zero label
	str r2, [r1]				; store tail

	b exit_no_error			; exit without error

check_head:
	ldr r2, hp				; load head pointer into r2
	ldr r2, [r2]
	cmp r2, #0				; if head == 0
	beq exit_error			; queue is full, exit with error
							; else
	mov r3, #0
	ldr r1, tp				; set tail to 0
	str r3, [r1]
	ldr r1, [r1]
							; and insert value at tail
	ldr r3, qp				; load queue pointer
	str r0, [r3, r1]			; store value in queue at position tail
	add r1, #4				; increment tail by one
	ldr r0, tp				; get tail pointer to r0
	str r1, [r0]				; store tail value to pointer
	ldr r0, ep				; load elements pointer
	ldr r1, [r0]
	add r1, #4				; increase elements by one
	str r1, [r0]				; store elements value

	b exit_no_error			; exit without error

set_tail_zero:
	mov r3, #0				; set head to 0
	str r3, [r1]				; store tail
	b exit_no_error			; exit without error

; ---------------------------------------------------------------------------
; Get value from Queue
;
; parameter is storage address to insert value to
; ---------------------------------------------------------------------------
queue_get:
	ldr r1, ep					; get elements pointer
	ldr r2, [r1]					; get value
	cmp r2, #0					; empty queue ?
	beq exit_error				; exit with error
								; else get value
	ldr r1, qp					; load queue pointer
	ldr r2, hp					; get head pointer
	ldr r3, [r2]
	ldr r4, [r1, r3]			; get value at position head
	str r4, [r0]				; store value to memory address

	ldr r0, ep					; get elements pointer
	ldr r1, [r0]				; get value
	sub r1, #4					; decrease value by one
	str r1, [r0]				; store elements value

	add r3, #4					; increment head by one
	cmp r3, #MAX_SIZE			; if head == MAX_SIZE
	beq	set_head_zero			; set head to 0
	str r3, [r2]				; else store head

	b exit_no_error				; exit without error

set_head_zero:
	mov r3, #0					; set head to 0
	str r3, [r2]				; store head
	b exit_no_error				; exit without error


; ---------------------------------------------------------------------------
; Check if queue is empty
; ---------------------------------------------------------------------------
queue_empty:
	ldr r0, ep				; get elements pointer
	ldr r1, [r0]			; get value
	cmp r1, #0				; if elemets == 0
	beq exit_no_error		; return true
	b exit_error			; else return false


; ---------------------------------------------------------------------------
; Exit with error and return to caller
; ---------------------------------------------------------------------------
exit_error:				; exit code 0 = false
	mov r0, #0
	bx lr


; ---------------------------------------------------------------------------
; Exit without error and return to caller
; ---------------------------------------------------------------------------
exit_no_error:			; exit code 1 = true
	mov r0, #1
	bx lr
