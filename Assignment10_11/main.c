#include "msp.h"
#include "driverlib/driverlib.h"

// enumeration representing all possible states
enum
{
    STATE_ON, STATE_OFF, MAX_STATES
};

// enumeration representing all possible events
enum
{
    BUTTON_PRESSED, MAX_EVENTS
};

// structure defining a transition between states
typedef struct
{
    void (*action)();
    int nextState;
} Transition;

// turns the led on
void turn_led_on()
{
    P2->OUT |= 0x10;
}

// turns the led off
void turn_led_off()
{
    P2->OUT &= ~0x10;
}

// changes the state and calls the corresponding output function
void state_machine(int event)
{
    // the initial state of the state machine
    static int state = STATE_OFF;

    // the state machine table
    static Transition stateTable[MAX_EVENTS][MAX_STATES] = { { { turn_led_off,
                                                                 STATE_OFF },
                                                               { turn_led_on,
                                                                 STATE_ON } }, }; // BUTTON_PRESSED

    if (state < MAX_STATES && event < MAX_EVENTS
            && stateTable[event][state].action != 0)
    {
        stateTable[event][state].action();
        state = stateTable[event][state].nextState;
    }
}

void main(void)
{
    // setting watchdog
    MAP_WDT_A_initIntervalTimer(WDT_A_CLOCKSOURCE_SMCLK,
    WDT_A_CLOCKITERATIONS_32K);
    WDT_A_startTimer();

    // reset green led
    P2->DIR |= 0x10;
    P2->OUT &= ~0x10;

    // initialise the event queue
    queue_init();

    // at a clock frequency of 3Mhz, with a counter of 30.000 the sampling speed is 10ms
    SysTick_Config(30000);

    for (;;)
    {
        if (!queue_empty())
        {
            int event;
            if (queue_get(&event) == 1)
            {
                state_machine(event);
            }
        }
        WDT_A_clearTimer();
    }
}

// System clock interrupt
void SysTick_Handler(void)
{
    static int old;

    //retrieves the value of P3IN.5 GPIO pin
    int cur = !BITBAND_PERI(P3->IN, 5);

    if (cur != old && cur == 1)
    {
        old = cur;
        queue_put(BUTTON_PRESSED);
    }
    old = cur;
}

