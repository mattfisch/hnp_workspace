#include <application/application.h>
#include <hardware/hal.h>
#include "statemachine/smf.h"

int main()
{
    Application app;
    app.run();
}
