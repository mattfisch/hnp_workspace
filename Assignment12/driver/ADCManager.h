#ifndef DRIVER_ADCMANAGER_H_
#define DRIVER_ADCMANAGER_H_

#include "statemachine/smf.h"

class ADCManager: public SM
{
public:
    ADCManager();
    virtual ~ADCManager();
    void init();
    void handleEvent(Event evt);

private:
    Timer timer;
};

#endif
