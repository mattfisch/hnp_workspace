#ifndef DRIVER_SENSOR_GRADIENTSENSOR_H_
#define DRIVER_SENSOR_GRADIENTSENSOR_H_

#include "statemachine/smf.h"

class GradientSensor : public SM
{
public:
    GradientSensor();
    ~GradientSensor();
    void init();
    void handleEvent(Event evt);
private:
    int value;
    int oldValue;
};
#endif
