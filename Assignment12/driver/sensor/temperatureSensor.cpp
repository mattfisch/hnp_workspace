#include <driver/sensor/temperatureSensor.h>
#include "driver/ADCManager.h"
#include "hardware/hal.h"

TemperatureSensor::TemperatureSensor() :
        value(0), oldValue(0)
{
    SMF::subscribe(TEMP_MEASUREMENT_READY, this);
}

TemperatureSensor::~TemperatureSensor()
{
}

void TemperatureSensor::init()
{
    //nothing to do here
}

void TemperatureSensor::handleEvent(Event evt)
{
    int adcRefTempCal_1_2v_30 = TLV->ADC14_REF1P2V_TS30C;
    int adcRefTempCal_1_2v_85 = TLV->ADC14_REF1P2V_TS85C;
    value = (((int) MAP_ADC14_getResult(ADC_MEM0) - adcRefTempCal_1_2v_30)
            * (85 - 30) * 1000)
            / (adcRefTempCal_1_2v_85 - adcRefTempCal_1_2v_30) + (30 * 1000);
    if (value != oldValue)
    {
        SMF::postEvent(TEMP_UPDATE_EVENT, value);
        oldValue = value;
    }
}

