#ifndef DRIVER_TEMPERATURESENSOR_H_
#define DRIVER_TEMPERATURESENSOR_H_

#include "statemachine/smf.h"

class TemperatureSensor: public SM
{
public:
    TemperatureSensor();
    ~TemperatureSensor();
    void init();
    void handleEvent(Event evt);
private:
    int value;
    int oldValue;
};

#endif
