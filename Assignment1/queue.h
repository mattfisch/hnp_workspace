/*
 * queue
 *
 *  Created on: 8 May 2017
 *      Author: Matthias Fischbacher
 */

#ifndef QUEUE_H_
#define QUEUE_H_
#include <stddef.h>

template <class T, size_t size = 16> class Queue {

private:
    size_t head;
    size_t tail;
    T buffer[size];
    bool swapped;

public:
    Queue() : head(0), tail(0), swapped(false){}

    bool put(const T& t)
    {
        if(tail == size && head == 0)
        {
           return false;
        }
        else if(tail == size)
        {
            tail = 0;
            swapped = true;
        }

        if(swapped && tail == head)
            return false;

        buffer[tail++] = t;
        return true;
    }

    bool get(T& t)
    {
        if(!empty())
        {
            if(head == (size-1))
            {
                t = buffer[head];
                head = 0;
                swapped = false;
            }
            else
            {
                t = buffer[head++];
            }
            return true;
        }
        return false;
    }

    bool empty()
    {
        return head == tail && !swapped;
    }

};
#endif
