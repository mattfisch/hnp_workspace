//*****************************************************************************
//
// MSP432 main.c template - Empty main
//
//****************************************************************************

#include <assert.h>
#include "queue.h"

int main(void)
{
    Queue<int,5> q;
    int v;

    assert(q.empty());

    q.put(1);
    q.put(2);
    q.put(3);
    q.put(4);
    q.put(5);

    assert(q.put(6) == false);

    assert(q.get(v)  && v == 1);

    assert(q.put(11) == true);

    assert(q.get(v) && v == 2);
    assert(q.get(v) && v ==  3);

    assert(q.put(22) == true);
    assert(q.put(33) == true);

    assert(q.get(v) && v ==  4);
    assert(q.get(v) && v ==  5);

    assert(q.put(44) == true);
    assert(q.put(55) == true);

    assert(q.get(v) && v ==  11);
    assert(q.get(v) && v ==  22);

    assert(q.put(111) == true);
    assert(q.put(222) == true);

    assert(q.get(v) && v ==  33);
    assert(q.get(v) && v ==  44);
    assert(q.get(v) && v ==  55);
    assert(q.get(v) && v ==  111);
    assert(q.get(v) && v ==  222);

    assert(q.empty());

    return 0;
}
